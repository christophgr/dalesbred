package fi.evident.dalesbred.instantiation.test;

import fi.evident.dalesbred.Reflective;

class InaccessibleClass {

    @Reflective
    public InaccessibleClass(int x) { }
}

